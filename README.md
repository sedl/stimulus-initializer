## What it does

Handles the initialization process like [described on stimulus](https://stimulus.hotwired.dev/handbook/hello-stimulus)

Initializes all stimulus controllers, recursively within a given folder, based on matched naming (e.g. `*-controller.js`).

## Using example

With an initializing javascript file, example like rails / Vite

`frontend/entrypoints/application.js`

```
import { initStimulus } from "@csedl/stimulus-initializer";

const comps = import.meta.glob('../javascript/components/**/*-controller.js', { eager: true })
initStimulus(comps, { debug: true })
```

Output to the Javascript console:

```
STIMULUS IDENTIFIER «dropdown» from: ../javascript/components/any/deeply/nested/path/dropdown-controller.js
```

The initializer always removes the `-controller.js` prefix from the filename (hardcoded, but could just be optional).

This will be connected to any element like:

```html
<div data-controller="dropdown"></div>
```

This means that `dropdown` is the ID and must be unique over all folders!

The initializer checks for uniqueness and throws an error if it is not.

It is possible to run the initializer multiple times on different folders. The uniqueness check is independent of this.

## Requirements

You must have `@hotwired/stimulus` installed on your system.
