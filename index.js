import {Application} from "@hotwired/stimulus"

window.Stimulus = Application.start()

    var initialized_controllers = []

export function initStimulus(imports, options = {}) {

    const paths = Object.keys(imports)

    console.log('START initializing controller...')

    for (let index = 0; index < paths.length; ++index) {

        const path = paths[index]
        const tn = path.split('/').slice(-1)[0]
        const identifier = tn.match(/^[\s\S]+(?=-controller.js)/)[0]

        if (options.debug) {
            console.log('STIMULUS IDENTIFIER «' + identifier + '» from: ' + path)
        }


        // check

        if (initialized_controllers.includes(identifier)) {
            const err = "NAMING CONFLICT STIMULUS\nDouble identifier: «" + identifier + "»\n\nfrom:\n" + path
            console.error(err)
            alert(err)
        } else {
            initialized_controllers.push(identifier)
        }

        const app = imports[path].default;
        Stimulus.register(identifier, app)

    }

    return('SUCCESS')

}